import sys
import subprocess

if len(sys.argv) > 1:

    # Setting ROI
    if sys.argv[1] == "--set-roi":
        run_command_speed_test_config = "cd speed_track && python speed_track.py --set-roi"
        print "Configure ROI"
        print "--------------------------------------------------"
        subprocess.call(run_command_speed_test_config, shell=True)

    # Generate det.npy
    elif sys.argv[1] == "--generate":
        run_command_to_generate_npy = "python generate_detections.py --model=./resources/networks/mars-small128.ckpt-68577 --mot_dir=./data/ --output_dir=./resources/detections/"
        print "Generating npy"
        print "--------------------------------------------------"
        subprocess.call(run_command_to_generate_npy, shell=True)

# Runs tracking, speed estimation and plots
else:
    run_command_deep_sort_app = "python deep_sort_app.py --sequence_dir=./data/ --detection_file=./resources/detections/det.npy --min_confidence=0.5 --nn_budget=100 --output_file=./output/deep-sort-output.txt"
    run_command_speed_test = "cd speed_track && python speed_track.py"
    run_command_draw = "python plot_output.py ./data/ ./speed_track"
    print "Start Tracking"
    print "--------------------------------------------------"
    subprocess.call(run_command_deep_sort_app, shell=True)

    print "Estimating Speed"
    print "--------------------------------------------------"
    subprocess.call(run_command_speed_test, shell=True)

    print "Displaying video with track_id and speed estimated"
    print "--------------------------------------------------"
    subprocess.call(run_command_draw, shell=True)

