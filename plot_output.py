import sys
import os
import cv2
import numpy as np
import colorsys
import configparser
import time
import datetime
import subprocess



# read config.ini to get roi and threshold
config = configparser.ConfigParser()
config.read('./speed_track/config.ini')

try:
    coordinates = config['ROI']
    speed_limit = int(coordinates['speed_limit'])
except:
    print "set threshold"

# Assigning image folder and det.txt file to a variable
images = sys.argv[1]
det_txt = sys.argv[2]

# creating a folder with local timestamp to store cars which moves over speed limit
ts = time.time()
st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d_%H:%M:%S')

print "created folder " + st
subprocess.call("cd output_images && mkdir " + st, shell=True)


# Loading det.txt to numpy
det_file = np.loadtxt(det_txt+'/speed-deep-sort-output.txt', delimiter=',')

#  Assigning unique color to a track_id
def unique_color_to_track_id(track_id, hue_step=0.41) :
    h, v = (track_id * hue_step) % 1, 1. - (int(track_id * hue_step) % 4) / 5.
    r, g, b = colorsys.hsv_to_rgb(h, 1., v)
    return r, g, b

# plotting frames by taking coordinates from det.txt file
for file in sorted(os.listdir(images+'img1')):
    filename_without_extension = os.path.splitext(file)[0]
    image_path = images+'img1/'+file
    frame = cv2.imread(image_path)
    frame_index = np.where(det_file[:, 0] == int(filename_without_extension))
    for x in xrange(det_file[frame_index].shape[0]):
        track_id = int(det_file[frame_index][x][1])
        track_id_speed = int(det_file[frame_index][x][10])

        # getting r, g, b for particular track_id
        r, g, b = unique_color_to_track_id(track_id)

        # drawing bounding box
        pt1 = int(det_file[frame_index][x][2]), int(det_file[frame_index][x][3])
        pt2 = int(pt1[0] + det_file[frame_index][x][4]), int(pt1[1] + det_file[frame_index][x][5])
        if track_id_speed >= speed_limit:
            cv2.rectangle(frame, pt1, pt2, (0, 0, 255), 6)
            filename = "./output_images/" + st + "/" + file
            cv2.imwrite(filename, frame)
        else:
            cv2.rectangle(frame, pt1, pt2, (255 * r, 255 * g, 255 * b), 2)

        text_size = cv2.getTextSize(str(track_id), cv2.FONT_HERSHEY_PLAIN, 1, 2)

        # drawing tracking box
        pt2 = pt1[0] + 10 + text_size[0][0], pt1[1] + 10 + text_size[0][1]
        cv2.rectangle(frame, pt1, pt2, (255, 0, 0), -1)

        center = int(pt1[0]) + 5, int(pt1[1]) + 5 + text_size[0][1]

        cv2.putText(frame, str(track_id), center, cv2.FONT_HERSHEY_PLAIN, 1, (255, 255, 255), 2)

        # drawing speed box
        if track_id_speed != 0:
            pt1 = pt1[0] + 35, pt1[1]

            pt2 = pt1[0] + 10 + text_size[0][0], pt2[1]

            center = pt1[0] + 5, pt1[1] + 5 + text_size[0][1]
            cv2.rectangle(frame, pt1, pt2, (255, 122, 0), -1)

            cv2.putText(frame, str(track_id_speed), center, cv2.FONT_HERSHEY_PLAIN, 1, (255, 255, 255), 2)

        cv2.imshow("High Speed Car Detection Demo", frame)
        if track_id_speed >= speed_limit:
            filename = "./output_images/" + st + "/" + file
            cv2.imwrite(filename, frame)
        cv2.waitKey(2)
print "Frames where cars moving over speed limit are stored in ./output_images/" + st

