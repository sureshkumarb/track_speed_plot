This repository consists of 2 modules concerned with tracking as of now:

1. SORT
2. Deep SORT

Below is the interface elaborated for both.

## Simple Real Time Tracker based on Kalman Filters

This is a forked implementation of SORT: https://github.com/abewley/sort

### Running SORT for tracking

### Dependencies:

0. [`scikit-learn`](http://scikit-learn.org/stable/)
0. [`scikit-image`](http://scikit-image.org/download)
0. [`FilterPy`](https://github.com/rlabbe/filterpy)

```
$ pip search filterpy
```

### Demo:

To run the tracker with the provided detections:

```
$ cd path/to/sort
$ python sort.py
```

To display the results you need to:

1. Download detection dataset: <paste Google Drive link>
2. Place dataset in <path_to_high-speed-car-detection>/data/
3. Run the demo with the ```--display``` flag
  ```
  $ python sort.py --display
  ```

# Deep SORT

This is a forked implementation of SORT: https://github.com/nwojke/deep_sort

## Dependencies

The code is compatible with Python 2.7 and 3. The following dependencies are
needed to run the tracker:

* NumPy
* sklean
* OpenCV
* TensorFlow (>= 1.0)

## Running the tracker

Download detection dataset: <paste Google Drive link>

## Generating detections

Beside the main tracking application, this repository contains a script to
generate features for re-identification, suitable to compare the visual
appearance of bounding boxes using cosine similarity.
The following example generates these features from standard MOT challenge
detections. 

```
python generate_detections.py
    --model=resources/networks/mars-small128.ckpt \
    --mot_dir=<path_to_high-speed-car-detection>/data/
    --output_dir=./resources/detections/
    
```

Using the generated detections, use the following to run deep sort:

```
python deep_sort_app.py \
    --sequence_dir=path_to_high-speed-car-detection/data/
    --detection_file= ./resources/detections/<file>.npy \
    --min_confidence=0.3 \
    --nn_budget=100 \
    --display=True
```

**Commands to execute after integrating tracking and speed estimations**

**Setup before executing below commands**

Copy "det.txt" to "data/det/" and all the frames to "data/img1"

Copy one sample frame for drawing roi to speed_track folder and name that sample frame as "frame_to_set_roi.jpg"


**Step 1:** Set ROI

```
python track_speed_plot.py --set-roi

```
**Step 2:** Generate det.npy file

```
python track_speed_plot.py --generate

```

**Step 3:** Run Track, Estimate Speed and Plot

```
python track_speed_plot.py

```

Frames where cars moving over speed limit are stored in "./output_images/<time_stamp>" folder
