import sys, time

from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import numpy as np
import cv2
import configparser

import draw_custom_roi


def init_set_roi():
    frame = cv2.imread('frame_to_set_roi.jpg', -1)
    time.sleep(0.25)
    copy_image = frame.copy()
    draw_custom_roi.define_rectangle(copy_image, frame)


def get_roi_points(select_polygon):
    # global coordinates
    boundary_points = []
    try:
        if (select_polygon == -1):
            lower_left_x = int(coordinates['bottom_lower_left_x'])
            lower_left_y = int(coordinates['bottom_lower_left_y'])
            upper_left_x = int(coordinates['bottom_upper_left_x'])
            upper_left_y = int(coordinates['bottom_upper_left_y'])
            upper_right_x = int(coordinates['bottom_upper_right_x'])
            upper_right_y = int(coordinates['bottom_upper_right_y'])
            lower_right_x = int(coordinates['bottom_lower_right_x'])
            lower_right_y = int(coordinates['bottom_lower_right_y'])

        elif (select_polygon == 0):
            lower_left_x = int(coordinates['lower_left_x'])
            lower_left_y = int(coordinates['lower_left_y'])
            upper_left_x = int(coordinates['upper_left_x'])
            upper_left_y = int(coordinates['upper_left_y'])
            upper_right_x = int(coordinates['upper_right_x'])
            upper_right_y = int(coordinates['upper_right_y'])
            lower_right_x = int(coordinates['lower_right_x'])
            lower_right_y = int(coordinates['lower_right_y'])

        elif (select_polygon == 1):
            lower_left_x = int(coordinates['top_lower_left_x'])
            lower_left_y = int(coordinates['top_lower_left_y'])
            upper_left_x = int(coordinates['top_upper_left_x'])
            upper_left_y = int(coordinates['top_upper_left_y'])
            upper_right_x = int(coordinates['top_upper_right_x'])
            upper_right_y = int(coordinates['top_upper_right_y'])
            lower_right_x = int(coordinates['top_lower_right_x'])
            lower_right_y = int(coordinates['top_lower_right_y'])

        else:
            return boundary_points

        boundary_points.append((lower_left_x, lower_left_y))
        boundary_points.append((upper_left_x, upper_left_y))
        boundary_points.append((upper_right_x, upper_right_y))
        boundary_points.append((lower_right_x, lower_right_y))

        return boundary_points

    except KeyError:
        print '*** ERROR: Unable to parse config.ini ***'
        print 'Try running with \'--set-roi\' argument\n'
        sys.exit(1)


def get_centroid(bounding_box):
    # print bounding_box
    # find centroid
    temp_box = np.hsplit(bounding_box, 2)
    temp_box[1] = temp_box[1] / 2
    temp_centroid = temp_box[0] + temp_box[1]
    # print temp_centroid
    return temp_centroid


def speed_calculate(total_frames_id, fps, distance):
    if total_frames_id > 0:
        total_time = float(total_frames_id / fps)
        print "total time: " + str(total_time)
        speed_in_ms = float(distance / total_time)
        # convert m/s to km/h
        speed_in_kmh = float(speed_in_ms * 3600 / 1000)
        return speed_in_kmh
    else:
        return -1


def get_speed():
    # create polygons with roi
    boundary_outer = get_roi_points(0)
    if (len(boundary_outer) == 0):
        print '*** ERROR: Unable to read config.ini ***'
        sys.exit(1)
    else:
        polygon_outer = Polygon(boundary_outer)

    boundary_inner_top = get_roi_points(1)
    if (len(boundary_inner_top) == 0):
        print '*** ERROR: Unable to read config.ini ***'
        sys.exit(1)
    else:
        polygon_inner_top = Polygon(boundary_inner_top)

    boundary_inner_bottom = get_roi_points(-1)
    if (len(boundary_inner_bottom) == 0):
        print '*** ERROR: Unable to read config.ini ***'
        sys.exit(1)
    else:
        polygon_inner_bottom = Polygon(boundary_inner_bottom)

    print "\npolygon_outer: " + str(polygon_outer)
    print "polygon_inner_top: " + str(polygon_inner_top)
    print "polygon_inner_bottom: " + str(polygon_inner_bottom) + '\n'

    # load sample output text file as numpy array
    array = np.loadtxt('../output/deep-sort-output.txt', delimiter=',')

    print "Array dimension: ", array.shape
    '''
    z = np.zeros((array.shape[0], 1), dtype='int64')
    array = np.append(array, z, axis=1)
    '''
    # extract only useful columns
    req_data = array[0:, :6]
    print '--------------------------------------------------------------'
    print "\nExtracted numpy array\n", req_data

    # create object id array
    obj_id_array = req_data[:, 1]
    print "\nObject ID Array\n", obj_id_array

    # find maximum number in object id column
    obj_id_max = np.max(obj_id_array)
    print "\nMax objects tracked: ", obj_id_max

    # create a temp numpy array
    temp_numpy_row_altered = False
    temp_filtered_array = np.zeros((1, 6))

    # get indices of object ids, extract bounding box for each id and do speed estimation
    for i in range(1, int(obj_id_max + 1)):

        # global obj_id_indices

        obj_id_indices = np.where(obj_id_array == i)[0]
        # print "\nObject ID {} and BB are {} and size is {}".format(i, objIdIndices, objIdIndices.size)

        # if an object id is not present in object id array, just skip it
        if (obj_id_indices.size == 0):
            continue

        # extract all bounding boxes for each object id, find centroid, count number
        # of frames and do speed estimation
        for j in range(0, obj_id_indices.size):

            # print objIdIndices[j]
            point = Point(get_centroid(req_data[obj_id_indices[j], 2:6]))
            # if centroid lies inside the polygon, increment the frame count
            if not (polygon_outer.contains(point)):
                continue

            if (temp_numpy_row_altered == False):
                temp_filtered_array[0] = req_data[obj_id_indices[j], :6]
                temp_numpy_row_altered = True
            else:
                temp_filtered_array = np.vstack([temp_filtered_array, req_data[obj_id_indices[j], :6]])

    print "\ntemp numpy filtered array\n"
    print temp_filtered_array

    temp_obj_id_array = temp_filtered_array[:, 1]
    print "\nTotal frames ID Array\n", temp_obj_id_array

    m = 0
    for i in range(1, int(obj_id_max + 1)):

        temp_obj_id_indices = np.where(temp_obj_id_array == i)[0]
        n = temp_obj_id_indices.size

        if (n == 0):
            continue
        # print 'object id {} and indices are from {} to {}'.format(i, m, m + n - 1)

        temp_point1 = Point(get_centroid(temp_filtered_array[m, 2:6]))
        temp_point2 = Point(get_centroid(temp_filtered_array[m + n - 1, 2:6]))

        m = m + n

        if not (polygon_inner_top.contains(temp_point1) and polygon_inner_bottom.contains(temp_point2) or
                        polygon_inner_bottom.contains(temp_point1) and polygon_inner_top.contains(temp_point2)):
            continue

        print "frame count of id {} is {}".format(i, n)
        frame_count = n
        # if frame count is > 0, calculate the total time taken and do speed estimation
        # s = d/t m/s
        speed = speed_calculate(frame_count, FRAME_RATE, DISTANCE)
        if (speed != -1):
            print "\nSpeed Estimation of obj id {} is {} kmph".format(i, speed)

            # write speed information to last column of the respective object id

            temp_obj_id_indices = np.where(obj_id_array == i)[0]
            for k in temp_obj_id_indices:
                array[k][10] = speed

    np.savetxt('speed-deep-sort-output.txt', array, delimiter=',')  # X is an array

    print "speed estimation is done..."


def main():
    global coordinates, DISTANCE, FRAME_RATE
    # construct the argument parser and parse the arguments
    if len(sys.argv) > 1:
        if sys.argv[1] == '--set-roi':
            init_set_roi()
            sys.exit(0)

        else:
            print '\n*** ERROR: Unknown argument passed ***'
            print '\nProgram terminated'
            sys.exit(0)

    try:
        config = configparser.ConfigParser()
        config.read('config.ini')
        coordinates = config['ROI']
        DISTANCE = float(coordinates['distance'])
        FRAME_RATE = float(coordinates['frame_rate'])

        print "DISTANCE:", DISTANCE
        print "FRAME_RATE:", FRAME_RATE

    except KeyError:
        print '\n*** ERROR: Unable to parse config.ini ***'
        print 'Try running with \'--set-roi\' argument\n'
        sys.exit(1)


if __name__ == "__main__":
    main()
    get_speed()
