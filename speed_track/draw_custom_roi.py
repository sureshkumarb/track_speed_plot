import time
import sys

import cv2
import configparser
from shapely.geometry import LineString

drawing_mode = False
drawing_box = False
ix,iy = -1,-1
fx,fy = -1, -1
rect = (0,0,1,1)
rect_pts = []
inner_rect_pts = []
distance = 0
frame_rate = 0
speed_limit = 0
DIVIDING_FACTOR = 4


def parse_data(d):
    wkt_data = d.split()
    x = float(wkt_data[1].strip('('))
    y = float(wkt_data[2].strip(')'))
    x = int(round(x))
    y = int(round(y))
    return (x, y)


def draw_line(image, px, py, color, thickness):
    cv2.line(image, px, py, color ,thickness)


# mouse callback function to draw roi
def define_rectangle(copy_image, frame):

    global drawing_mode, distance, frame_rate, speed_limit

    def draw_rectangle(event, x, y, flags, param):

        global rect_pts, inner_rect_pts, ix, iy, fx, fy
        global drawing_mode, drawing_box, rect

        if event == cv2.EVENT_LBUTTONDOWN:
            if(drawing_mode == True):
                drawing_box = True
                ix,iy = x,y

        elif event == cv2.EVENT_LBUTTONUP:
            if(drawing_mode == True):
                drawing_box = False
                rect_pts.append((ix, iy))
                if(len(rect_pts) == 1):
                    pass
                elif(len(rect_pts) == 2):
                    draw_line(frame,rect_pts[0], rect_pts[1],(255,255,255),2)
                elif(len(rect_pts) == 3):
                    draw_line(frame,rect_pts[1], rect_pts[2],(0,255,255),2)
                elif(len(rect_pts) == 4):
                    draw_line(frame,rect_pts[2], rect_pts[3],(255,255,255),2)
                    draw_line(frame,rect_pts[3], rect_pts[0],(0,255,255),2)
                    draw_line(frame, rect_pts[3], rect_pts[0], (0, 255, 0), 1)
                    draw_line(frame, rect_pts[3], rect_pts[0], (0, 255, 0), 1)

                    print "\nouter rect", rect_pts
                    print "---------------------------------------------------------------------"
                    line1_distance = LineString([rect_pts[0], rect_pts[1]])
                    print "Y Distance of left line using shapely: " + str(line1_distance.length)

                    y1_threshold = int(line1_distance.length/DIVIDING_FACTOR)
                    p1_bottom = LineString([rect_pts[0], rect_pts[1]]).interpolate(y1_threshold)
                    (x1_bottom, y1_bottom) = parse_data(p1_bottom.wkt)
                    inner_rect_pts.append((x1_bottom, y1_bottom))
                    print "bottom left P: ", (x1_bottom, y1_bottom)

                    p1_up = LineString([rect_pts[1], rect_pts[0]]).interpolate(y1_threshold)
                    (x1_up, y1_up) = parse_data(p1_up.wkt)
                    inner_rect_pts.append((x1_up, y1_up))
                    print "up left P: ", (x1_up, y1_up)

                    line2_distance = LineString([rect_pts[3], rect_pts[2]])
                    print "Y Distance of left line using shapely: " + str(line2_distance.length)

                    y2_threshold = int(float(line2_distance.length / DIVIDING_FACTOR))
                    p2_up = LineString([rect_pts[2], rect_pts[3]]).interpolate(y2_threshold)
                    (x2_up, y2_up) = parse_data(p2_up.wkt)
                    inner_rect_pts.append((x2_up, y2_up))
                    print "up right P: ", (x2_up, y2_up)

                    p2_bottom = LineString([rect_pts[3], rect_pts[2]]).interpolate(y2_threshold)
                    (x2_bottom, y2_bottom) = parse_data(p2_bottom.wkt)
                    inner_rect_pts.append((x2_bottom, y2_bottom))
                    print "bottom right P: ", (x2_bottom, y2_bottom)

                    draw_line(frame, inner_rect_pts[0], inner_rect_pts[3], (255, 255, 204), 2)
                    draw_line(frame, inner_rect_pts[1], inner_rect_pts[2], (255, 255, 204), 2)
                    print "\ninner rect", inner_rect_pts
                    print "---------------------------------------------------------------------"
                    print "Press: \n'c': Confirm\n'r': Reset\n'q': Quit"



    drawing_mode = True
    cv2.namedWindow('calibration')
    cv2.setMouseCallback('calibration', draw_rectangle)
    print "Image resolution: ", frame.shape
    print "--------------------------------------------------------------"
    print "Define roi, select Left->Top->Right->Bottom points on an image"
    print "Please follow the below order while selecting roi"
    print "--------------------------------------------------------------"
    print "Select --> lower_left_x, lower_left_y  point"
    print "Select --> upper_left_x, upper_left_y  point"
    print "Select --> upper_right_x, upper_right_y  point"
    print "Select --> lower_right_x, lower_right_y  point"

    while(1):
        # frame = imutils.resize(frame, width=1000)
        cv2.imshow('calibration', frame)

        # wait for for c to be pressed
        key = cv2.waitKey(10) & 0xFF
        # if the 'c' key is pressed, break from the loop
        if key == ord("r"):
            frame = copy_image.copy()
            rect_pts[:] = []
            inner_rect_pts[:] = []
        elif key == ord("c"):
            cv2.imwrite( "roi_sample.jpg", frame)
            for i in range(0,4):
                cv2.destroyWindow('calibration')
                cv2.waitKey(1)
            print '\nroi sample image has been saved for future use as roi_sample.jpg'
            drawing_mode = False
            break
        elif key == ord("q"):
            for i in range(0,4):
                cv2.destroyWindow('calibration')
                cv2.waitKey(1)
            drawing_mode = False
            print '\n*** CALIBRATION NOT DONE: Operation cancelled by user ***'
            sys.exit(0)

    time.sleep(1)

    print("Monitored area : ")

    print(" lower_left_x {}".format(rect_pts[0][0]))
    print(" lower_left_y {}".format(rect_pts[0][1]))
    print(" upper_left_x {}".format(rect_pts[1][0]))
    print(" upper_left_y {}".format(rect_pts[1][1]))
    print(" upper_right_x {}".format(rect_pts[2][0]))
    print(" upper_right_y {}".format(rect_pts[2][1]))
    print(" lower_right_x {}".format(rect_pts[3][0]))
    print(" lower_right_y {}".format(rect_pts[3][1]))

    while True:
        try:
            distance = float(raw_input("\nPlease enter distance in meters: "))
        except ValueError:
            print("\nSorry, I didn't understand that. Try again.")
            continue

        if (distance < 0 or distance == 0):
            print("\nSorry, your response must not be negative or zero.")
            continue

        else:
            # distance was successfully parsed, and we're happy with its value.
            # we're ready to exit the loop.
            break

    while True:
        try:
            frame_rate = int(raw_input("\nPlease enter frame rate of video in fps: "))
        except ValueError:
            print("\nSorry, I didn't understand that. Try again.")
            continue

        if (frame_rate < 0 or frame_rate == 0):
            print("\nSorry, your response must not be negative or zero.")
            continue

        else:
            break

    while True:
        try:
            speed_limit = int(raw_input("\nPlease enter speed limit in kmph: "))
        except ValueError:
            print("\nSorry, I didn't understand that. Try again.")
            continue

        if (speed_limit < 0 or speed_limit == 0):
            print("\nSorry, your response must not be negative or zero.")
            continue
        else:
            break

    save_config_file()


def save_config_file():
    config = configparser.ConfigParser()
    config['ROI'] = {}
    config['ROI']['lower_left_x'] = str(rect_pts[0][0])
    config['ROI']['lower_left_y'] = str(rect_pts[0][1])
    config['ROI']['upper_left_x'] = str(rect_pts[1][0])
    config['ROI']['upper_left_y'] = str(rect_pts[1][1])
    config['ROI']['upper_right_x'] = str(rect_pts[2][0])
    config['ROI']['upper_right_y'] = str(rect_pts[2][1])
    config['ROI']['lower_right_x'] = str(rect_pts[3][0])
    config['ROI']['lower_right_y'] = str(rect_pts[3][1])

    config['ROI']['top_lower_left_x'] = str(inner_rect_pts[1][0])
    config['ROI']['top_lower_left_y'] = str(inner_rect_pts[1][1])
    config['ROI']['top_upper_left_x'] = str(rect_pts[1][0])
    config['ROI']['top_upper_left_y'] = str(rect_pts[1][1])
    config['ROI']['top_upper_right_x'] = str(rect_pts[2][0])
    config['ROI']['top_upper_right_y'] = str(rect_pts[2][1])
    config['ROI']['top_lower_right_x'] = str(inner_rect_pts[2][0])
    config['ROI']['top_lower_right_y'] = str(inner_rect_pts[2][1])

    config['ROI']['bottom_lower_left_x'] = str(rect_pts[0][0])
    config['ROI']['bottom_lower_left_y'] = str(rect_pts[0][1])
    config['ROI']['bottom_upper_left_x'] = str(inner_rect_pts[0][0])
    config['ROI']['bottom_upper_left_y'] = str(inner_rect_pts[0][1])
    config['ROI']['bottom_upper_right_x'] = str(inner_rect_pts[2][0])
    config['ROI']['bottom_upper_right_y'] = str(inner_rect_pts[2][1])
    config['ROI']['bottom_lower_right_x'] = str(rect_pts[3][0])
    config['ROI']['bottom_lower_right_y'] = str(rect_pts[3][1])

    config['ROI']['distance'] = str(distance)
    config['ROI']['frame_rate'] = str(frame_rate)
    config['ROI']['speed_limit'] = str(speed_limit)

    with open('config.ini', 'w') as configfile:
        config.write(configfile)
    print '\nconfig.ini has been created\n'
